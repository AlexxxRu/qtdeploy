#!/bin/bash
# Script requre:
# QMAKE_DIR variable
# QINSTALLER_DIR variable

INSTALLER_CONFIG_DIR="installer"
PROJECT_DEPENDECIES_DIR="dependencies"

TARGET_OS=$(if [ -z "$2" ]; then echo "win"; else echo $2; fi)
READ_LINK_COMMAND=$(if [ "$TARGET_OS" == "mac" ]; then echo "greadlink"; else echo "readlink"; fi)

TARGET_DIR=$(dirname $($READ_LINK_COMMAND -f  $1))
PROJECT_NAME=$(basename $1 | cut -d '.' -f1)

QT_DEPLOY_COMMAND=$QMAKE_DIR/$(if [ "$TARGET_OS" == "win" ]; then echo "windeployqt"; elif [ "$TARGET_OS" == "mac" ]; then echo "macdeployqt"; else echo "linuxdeployqt"; fi)
QT_DEPLOY_OPTIONS=$(if [ "$TARGET_OS" == "lin" ]; then echo "-qmake=$QMAKE_DIR/qmake"; else echo ""; fi)
QT_TARGET_POSTIFIX=$(if [ "$TARGET_OS" == "win" ]; then echo ".exe"; elif [ "$TARGET_OS" == "mac" ]; then echo ".app"; else echo ""; fi)

INITIAL_DIR=$PWD
DEPLOY_DIR="$PWD/$PROJECT_NAME""_deploy"
PROJECT_BUILD=$DEPLOY_DIR/build

INSTALLER_DIR=$DEPLOY_DIR/installer
INSTALLER_DIR_DATA=$INSTALLER_DIR/packages/$PROJECT_NAME/data
INSTALLER_DIR_META=$INSTALLER_DIR/packages/$PROJECT_NAME/meta

PROEJCT_PATH=$TARGET_DIR/$PROJECT_NAME.pro
PROJECT_PACKAGE=$TARGET_DIR/$INSTALLER_CONFIG_DIR/$PROJECT_NAME.xml
PROJECT_DEPENDECIES=$TARGET_DIR/$PROJECT_DEPENDECIES_DIR
INSTALLER_CONFIG=$TARGET_DIR/$INSTALLER_CONFIG_DIR/config.xml

function validate {
	if [ -f "$1" ]; then return; fi
	if [ -d "$1" ]; then return; fi
	
	echo $2
	exit
}

function make_dir {
	if [ -d "$1" ]; then rm -rf "$1"; fi

	mkdir -p $1
	validate $1 "Failed to create dir: $1"
}

validate $PROEJCT_PATH "Failed to recognize project file"
validate $PROJECT_PACKAGE "Failed to recognize project installer config"
validate $INSTALLER_CONFIG "Failed to recognize installer config"

echo "Project postfix 	"$QT_TARGET_POSTIFIX
echo "Deploy tool 		"$QT_DEPLOY_COMMAND
echo "Deploy options 		"$QT_DEPLOY_OPTIONS
echo "======="
echo "Deploy dir 		"$DEPLOY_DIR
echo "Project 		"$PROEJCT_PATH
echo "Package 		"$PROJECT_PACKAGE
echo "Config  		"$INSTALLER_CONFIG
echo "======="
echo "Installer dir 		"$INSTALLER_DIR
echo "======="
echo "Intaller path 		"$INITIAL_DIR/$PROJECT_NAME$QT_TARGET_POSTIFIX
echo ""

make_dir "$DEPLOY_DIR"
make_dir "$PROJECT_BUILD"

make_dir "$INSTALLER_DIR"
make_dir "$INSTALLER_DIR_DATA"
make_dir "$INSTALLER_DIR_META"

echo "Preparing make files..."
cd $PROJECT_BUILD
$QMAKE_DIR/qmake $QMAKE_OPTIONS "CONFIG+=release" "target.path=$INSTALLER_DIR_DATA" "INSTALLS+=target" $PROEJCT_PATH > /dev/null

echo "Building $PROJECT_NAME..."
make > /dev/null

echo "Installing $PROJECT_NAME to $INSTALLER_DIR_DATA..."
make install > /dev/null

echo "Deploying all dependecies..."
cd $INSTALLER_DIR_DATA
eval "$QT_DEPLOY_COMMAND $INSTALLER_DIR_DATA/$PROJECT_NAME$QT_TARGET_POSTIFIX $QT_DEPLOY_OPTIONS > /dev/null" 

if [ -d "$PROJECT_DEPENDECIES" ]; then
	echo "Deploying additional dependecies..."
	cp -r $PROJECT_DEPENDECIES/* $INSTALLER_DIR_DATA
fi

# Copy project installer config
cp $PROJECT_PACKAGE $INSTALLER_DIR_META/package.xml

echo "Creating installer $PROJECT_NAME$QT_TARGET_POSTIFIX..."
cd $INSTALLER_DIR
$QINSTALLER_DIR/binarycreator -c $INSTALLER_CONFIG -p packages $PROJECT_NAME$QT_TARGET_POSTIFIX > /dev/null

echo "Moving installer to $INITIAL_DIR/$PROJECT_NAME$QT_TARGET_POSTIFIX..."
rm -rf $INITIAL_DIR/$PROJECT_NAME$QT_TARGET_POSTIFIX
mv $PROJECT_NAME$QT_TARGET_POSTIFIX $INITIAL_DIR

echo ""
echo "Completed!"
echo ""
